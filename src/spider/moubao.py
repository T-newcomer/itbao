import time

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import requests

TB_LOGIN_URL = 'https://login.taobao.com/member/login.jhtml'
CHROME_DRIVER = './browser/chromedriver.exe'

class SessionException(Exception):
    """
    会话异常类
    """

    def __init__(self, message):
        super().__init__(self)
        self.message = message

    def __str__(self):
        return self.message


class SessionDriver:

    def __init__(self):
        self.browser = None

    def get_session(self, username, password, goods):
        try:
            self.__init_browser()
            self.__switch_to_password_mode()
            time.sleep(0.5)
            self.__write_username(username)
            time.sleep(0.5)
            self.__write_password(password)
            time.sleep(0.5)
            if self.__lock_exist():
                self.__unlock()
            self.__submit()
            # 提取cookie
            for i in self.browser.get_cookies():
                self.cookie[i['name']] = i['value']

            time.sleep(1)

        finally:
            self.__destroy_browser()

    def __switch_to_password_mode(self):
        """
        切换到密码模式
        :return:
        """
        if self.browser.find_element_by_id('J_QRCodeLogin').is_displayed():
            self.browser.find_element_by_id('J_Quick2Static').click()

    def __write_username(self, username):
        """
        输入账号
        :param username:
        :return:
        """
        username_input_element = self.browser.find_element_by_id('TPL_username_1')
        username_input_element.clear()
        username_input_element.send_keys(username)

    def __write_password(self, password):
        """
        输入密码
        :param password:
        :return:
        """
        password_input_element = self.browser.find_element_by_id("TPL_password_1")
        password_input_element.clear()
        password_input_element.send_keys(password)

    def __lock_exist(self):
        """
        判断是否存在滑动验证
        :return:
        """
        return self.__is_element_exist('#nc_1_wrapper') and self.browser.find_element_by_id(
            'nc_1_wrapper').is_displayed()

    def __unlock(self):
        """
        执行滑动解锁
        :return:
        """
        bar_element = self.browser.find_element_by_id('nc_1_n1z')
        ActionChains(self.browser).drag_and_drop_by_offset(bar_element, 350, 0).perform()
        time.sleep(0.5)
        self.browser.get_screenshot_as_file('error.png')
        if self.__is_element_exist('.errloading > span'):
            error_message_element = self.browser.find_element_by_css_selector('.errloading > span')
            error_message = error_message_element.text
            self.browser.execute_script('noCaptcha.reset(1)')
            raise SessionException('滑动验证失败, message = ' + error_message)

    def __submit(self):
        """
        提交登录
        :return:
        """
        self.browser.find_element_by_id('J_SubmitStatic').click()
        time.sleep(0.5)
        if self.__is_element_exist("#J_Message"):
            error_message_element = self.browser.find_element_by_css_selector('#J_Message > p')
            error_message = error_message_element.text
            raise SessionException('登录出错, message = ' + error_message)

    def __init_browser(self):
        """
        初始化selenium浏览器
        :return:
        """
        self.__config_proxy()
        options = Options()
        # options.add_argument("--headless")
        options.add_argument('--proxy-server=http://127.0.0.1:9000')
        self.browser = webdriver.Chrome(options=options)
        self.browser.implicitly_wait(3)
        self.browser.maximize_window()
        self.browser.get(TB_LOGIN_URL)

    def __config_proxy(self):
        import os
        from threading import Thread
        os.system('pip install mitmproxy')
        self.httpThread = Thread(target=lambda: os.system(r'mitmdump -s HttpProxy.py -p 9000'), name=r'HttpProxy')
        self.httpThread.start()

    def __destroy_browser(self):
        """
        销毁selenium浏览器
        :return:
        """
        if self.browser is not None:
            pass
            # self.browser.quit()

    def __is_element_exist(self, selector):
        """
        检查是否存在指定元素
        :param selector:
        :return:
        """
        try:
            self.browser.find_element_by_css_selector(selector)
            return True
        except NoSuchElementException:
            return False

    def get_Page(self, goods):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36',
        }
        req = requests.get(url=r'https://s.taobao.com/search?q=' + goods + '&s=', headers=headers,
                           verify=False)

        with open(r'src/spider/page_result/' + goods + 'result.html', 'a', encoding='utf-8') as f:
            f.write(req.text)

    @property
    def cookie(self):
        cookies = {}
        for cookie in self.browser.get_cookies():
            cookies[cookie['name']] = cookie['value']
        return cookies

def get_session(username, password,goods):
    return SessionDriver().get_session(username, password, goods)

if __name__ == '__main__':
    try:
        get_session(r'14786188816', r'wbydh555')
    except Exception:
        print('Something Error')
