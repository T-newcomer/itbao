from src.models import *
import os
import re
import pandas as pd
from pandas import DataFrame
from src.models import Product
import time


class ParseController():

    def __init__(self):
        self.url = r'src\spider\page_result\\'

    def colon_split(self, results):
        result = []
        for i in results:
            result.append((re.split(':', i)[1])[1:-1])
        return result

    def parser(self, target='surface book 2'):
        if str(target + '_result.html') in os.listdir(self.url):

            with open(str(self.url+ target + '_result.html'), 'r', encoding='utf-8') as f:
                html = f.read()

                tlt = re.findall(r'\"raw_title\"\:\".*?\"', html)
                loc = re.findall(r'\"item_loc\"\:\".*?\"', html)
                shop = re.findall(r'\"nick\"\:\".*?\"', html)
                sales = re.findall(r'\"view_sales\"\:\".*?\"', html)
                plt = self.colon_split(re.findall(r'\"view_price\"\:\"[\d\.]*\"', html))
                plt = list(map(float, plt))

                data = pd.DataFrame({'price': plt, 'title': self.colon_split(tlt), 'location': self.colon_split(loc),
                                     'shop': self.colon_split(shop[0:len(shop) - 1]), 'sales': self.colon_split(sales)})

                for index, row in data.iterrows():
                    pro = Product(Pkeyword=target, Pname=row['title'], Pprice=row['price'], Pshop=row['shop'],
                                  Pnum=row['sales'], location=row['location'],when=time.strftime("%Y-%m-%d", time.localtime()))
                    pro.save()
