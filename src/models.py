from django.db import models

# Create your models here.

class Product(models.Model):
    Pkeyword = models.CharField(max_length=128, null=False)
    Pname = models.CharField(max_length=256, null=False)
    Pprice = models.IntegerField(null=False)
    Pnum = models.CharField(max_length=128,null=False)
    Pshop = models.CharField(max_length=64,null=False)
    location = models.CharField(max_length=64, null=False)
    when = models.DateField(blank=True, default='', null=True)

class Comment(models.Model):
    Pid = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    comment = models.TextField(max_length=256,null=False)

class User(models.Model):
    Uname = models.CharField(max_length=16,null=False)
    Utel = models.CharField(max_length=16,null=False)
    Upassward = models.CharField(max_length=20,null=False,default="")


